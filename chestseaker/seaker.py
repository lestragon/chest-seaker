from flask import Blueprint, current_app, render_template, request
import requests
from . import riot_api

bp = Blueprint('seaker', __name__, url_prefix='/')


@bp.route("/", methods=('GET', 'POST'))
def seaker():
    if request.method == 'POST':
        region = request.form['region']
        summoner_name = request.form['summoner-name']
        data = riot_api.get_summoner_by_name(region, summoner_name)
        encrypted_summoner_id = data.get('id')
        champion_masteries_data = riot_api.get_champion_masteries_by_summoner(region, encrypted_summoner_id)

        # Recuperation de la liste des champions
        r = requests.get("http://ddragon.leagueoflegends.com/cdn/12.11.1/data/fr_FR/champion.json")
        champion_assets = r.json()
        champion_list= {}
        for champion in champion_assets["data"].values():
            champion_list[int(champion["key"])] = champion

        return render_template('seaker.html', champion_masteries=champion_masteries_data, champion_list=champion_list)

    return render_template('seaker.html')
    
    

