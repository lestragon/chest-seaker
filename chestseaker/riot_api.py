import requests
from flask import current_app
    
def get_summoner_by_name(region: str, summoner_name: str):
    riot_api_base_url: str = current_app.config.get("PLATEFORM_ROUTING_URL").get(region)
    api_url=f"{riot_api_base_url}/lol/summoner/v4/summoners/by-name/{summoner_name}"
    riot_token = current_app.config.get("RIOT_TOKEN")
    r = requests.get(url=api_url,headers={"X-Riot-Token":riot_token})
    return r.json()

def get_champion_masteries_by_summoner(region: str, encrypted_summoner_id: str):
    riot_api_base_url: str = current_app.config.get("PLATEFORM_ROUTING_URL").get(region)
    api_url=f"{riot_api_base_url}/lol/champion-mastery/v4/champion-masteries/by-summoner/{encrypted_summoner_id}"
    riot_token = current_app.config.get("RIOT_TOKEN")
    r = requests.get(url=api_url,headers={"X-Riot-Token":riot_token})
    return r.json()