from flask import Flask, redirect, url_for

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object('chestseaker.config')
    app.config.from_object('chestseaker.secrets')

    if test_config:
        app.config.from_mapping(test_config)

    from . import seaker
    app.register_blueprint(seaker.bp)
    
    return app