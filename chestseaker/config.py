PLATEFORM_ROUTING_URL : dict = {
    "BR1" : "https://br1.api.riotgames.com",
    "EUN1": "https://eun1.api.riotgames.com",
    "EUW1": "https://euw1.api.riotgames.com",
    "JP1" : "https://jp1.api.riotgames.com",
    "KR"  : "https://kr.api.riotgames.com",
    "LA1" : "https://la1.api.riotgames.com",
    "LA2" : "https://la2.api.riotgames.com",
    "NA1" : "https://na1.api.riotgames.com",
    "OC1" : "https://oc1.api.riotgames.com",
    "TR1" : "https://tr1.api.riotgames.com",
    "RU"  : "https://ru.api.riotgames.com",
}
REGION_ROUTING_URL : dict = {
    "AMERICAS" : "americas.api.riotgames.com",
    "ASIA"     : "asia.api.riotgames.com",
    "EUROPE"   : "europe.api.riotgames.com"
}
LANGUAGE = "fr_FR"
RIOT_CDN_URL="http://ddragon.leagueoflegends.com/cdn/"
RIOT_CDN_VERSION="12.11.1" # Can also be compute using https://ddragon.leagueoflegends.com/api/versions.json


