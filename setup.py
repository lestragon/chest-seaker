from setuptools import find_packages, setup

setup(
    name='chestseaker',
    version='0.1.0',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask==2.1.2',
        'requests==2.28.1'
    ],
)